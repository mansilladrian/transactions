# Django
from django import forms

# Models
from apps.cards.models import Card, CardType, Person


class CardForm(forms.ModelForm):
    """Card form"""
    class Meta:
        model = Card
        fields = '__all__'
        exclude = 'creator', 'author'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'number': forms.TextInput(attrs={'class': 'form-control'}),
            'due_date': forms.TextInput(attrs={'class': 'form-control datepicker', 'autocomplete': 'off'}),
            'limit': forms.TextInput(attrs={'class': 'form-control'}),
            'rate': forms.TextInput(attrs={'class': 'form-control'}),
            'holder': forms.TextInput(attrs={'class': 'form-control'}),
            'person': forms.Select(attrs={'class': 'form-control chosen-select'}),
            'brand': forms.Select(attrs={'class': 'form-control chosen-select'}),
        }


class BrandForm(forms.ModelForm):
    """Brand form"""
    class Meta:
        model = CardType
        fields = 'name', 'rate'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'rate': forms.TextInput(attrs={'class': 'form-control'}),
        }


class PersonForm(forms.ModelForm):
    """Person form"""
    class Meta:
        model = Person
        fields = 'name', 'last_name', 'address', 'dni'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'dni': forms.TextInput(attrs={'class': 'form-control'})
        }
