# Django
from django.urls import path, include

urlpatterns = [
    path('v1/', include('apps.cards.api.v1.urls')),
]
