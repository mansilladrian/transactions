# Django
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404

# Models
from apps.cards.api.v1.serializers import CardSerializer, TransactionSerializer
from apps.cards.models import Card, Transaction

# Rest Framework
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.response import Response


class CardDetail(APIView):
    """Returns all data from a specific Card, filter by ID"""
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, pk):
        try:
            return Card.objects.get(pk=pk)
        except Card.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        card = self.get_object(kwargs.get('pk'))
        serializer = CardSerializer(card)
        return Response(serializer.data)

    # @action(detail=True, methods=['get'])
    # def is_valid(self, request, *args, **kwargs):
    #     """Check if the card is valid"""
    #     card = self.get_object()
    #     return Response(card.is_valid)


class CheckTransaction(RetrieveAPIView, GenericAPIView):
    """Receive transaction uuid and return if transaction is valid"""
    permission_classes = [permissions.IsAuthenticated]
    queryset = Transaction.objects.all()
    lookup_url_kwarg = 'uuid'
    lookup_field = 'uuid'
    serializer_class = TransactionSerializer

    @action(detail=True, methods=['get'])
    def is_valid(self, request, *args, **kwargs):
        return Response(self.get_object().is_valid)

