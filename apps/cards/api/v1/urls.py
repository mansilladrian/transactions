# Django

from django.urls import path

from apps.cards.api.v1.api import CardDetail, CheckTransaction

urlpatterns = [
    path('cards/<int:pk>', CardDetail.as_view(), name='card_detail'),
    path('transactions/<uuid:uuid>', CheckTransaction.as_view(), name='check_transaction'),
    # path('cards/<int:pk>/is_valid', CardDetail.as_view(), name='card_valid'),
]
