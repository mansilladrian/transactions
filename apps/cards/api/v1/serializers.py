from rest_framework import serializers

from apps.cards.models import Card, Person, Transaction


class PersonSerializer(serializers.ModelSerializer):
    """Model Person serializer"""

    class Meta:
        model = Person
        fields = 'name', 'last_name'


class CardSerializer(serializers.ModelSerializer):
    """Model Card serializer"""
    person = PersonSerializer()

    class Meta:
        model = Card
        fields = 'name', 'number', 'due_date', 'limit', 'rate', 'holder', 'person', 'brand'


class TransactionSerializer(serializers.ModelSerializer):
    """Return transaction's is_valid field"""

    class Meta:
        model = Transaction
        fields = 'rate', 'card_brand', 'amount'
