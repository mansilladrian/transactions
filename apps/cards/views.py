# Django
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView, ListView, CreateView, UpdateView

# Models
from apps.cards.forms import CardForm, BrandForm, PersonForm
from apps.cards.models import Card, CardType, Person, Transaction


class Dashboard(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard.html'


class ListCards(LoginRequiredMixin, ListView):
    template_name = 'cards/list_cards.html'
    model = Card


class NewCard(LoginRequiredMixin, CreateView):
    model = Card
    template_name = 'cards/generic_form.html'
    form_class = CardForm
    success_url = reverse_lazy('list_cards')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(NewCard, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(NewCard, self).get_context_data(**kwargs)
        context['reverse_url'] = self.success_url
        context['title'] = _('New card')
        context['breadcrumbs'] = [
            {'url': self.success_url, 'name': 'Cards'}
        ]
        return context


class UpdateCard(LoginRequiredMixin, UpdateView):
    model = Card
    template_name = 'cards/generic_form.html'
    form_class = CardForm
    success_url = reverse_lazy('list_cards')

    def get_initial(self):
        return {
            'due_date': self.object.due_date.strftime('%d/%m/%Y')
        }

    def get_context_data(self, **kwargs):
        context = super(UpdateCard, self).get_context_data(**kwargs)
        context['reverse_url'] = self.success_url
        context['title'] = _('Update card')
        context['breadcrumbs'] = [
            {'url': self.success_url, 'name': 'Cards'}
        ]
        return context


class ListBrands(LoginRequiredMixin, ListView):
    template_name = 'cards/list_card_type.html'
    model = CardType


class NewBrand(LoginRequiredMixin, CreateView):
    model = CardType
    template_name = 'cards/generic_form.html'
    form_class = BrandForm
    success_url = reverse_lazy('list_brands')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(NewBrand, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(NewBrand, self).get_context_data(**kwargs)
        context['reverse_url'] = self.success_url
        context['title'] = _('New brand')
        context['breadcrumbs'] = [
            {'url': self.success_url, 'name': 'Brands'}
        ]
        return context


class UpdateBrand(LoginRequiredMixin, UpdateView):
    model = CardType
    template_name = 'cards/generic_form.html'
    form_class = BrandForm
    success_url = reverse_lazy('list_brands')

    def get_context_data(self, **kwargs):
        context = super(UpdateBrand, self).get_context_data(**kwargs)
        context['reverse_url'] = self.success_url
        context['title'] = _('Update brand')
        context['breadcrumbs'] = [
            {'url': self.success_url, 'name': 'Brands'}
        ]
        return context


class ListPeople(LoginRequiredMixin, ListView):
    model = Person
    template_name = 'cards/list_people.html'


class NewPerson(LoginRequiredMixin, CreateView):
    model = Person
    template_name = 'cards/generic_form.html'
    form_class = PersonForm
    success_url = reverse_lazy('list_people')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(NewPerson, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(NewPerson, self).get_context_data(**kwargs)
        context['reverse_url'] = self.success_url
        context['title'] = _('New person')
        context['breadcrumbs'] = [
            {'url': self.success_url, 'name': 'People'}
        ]
        return context


class UpdatePerson(LoginRequiredMixin, UpdateView):
    model = Person
    template_name = 'cards/generic_form.html'
    form_class = PersonForm
    success_url = reverse_lazy('list_people')

    def get_context_data(self, **kwargs):
        context = super(UpdatePerson, self).get_context_data(**kwargs)
        context['reverse_url'] = self.success_url
        context['title'] = _('Update person')
        context['breadcrumbs'] = [
            {'url': self.success_url, 'name': 'People'}
        ]
        return context


class ListPersonCards(LoginRequiredMixin, ListView):
    """List cards of person"""
    model = Card
    template_name = 'cards/list_person_cards.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ListPersonCards, self).get_context_data(**kwargs)
        context['person'] = Person.objects.get(id=self.kwargs['pk'])
        return context

    def get_queryset(self):
        return Card.objects.filter(person_id=self.kwargs['pk'])


class ListTransactions(LoginRequiredMixin, ListView):
    """List transactions"""
    model = Transaction
    template_name = 'cards/list_transactions.html'


