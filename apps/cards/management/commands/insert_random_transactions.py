# Django
from django.core.management import BaseCommand

# Models
from apps.cards.models import Transaction, Person

# Utils
from random import randrange

from apps.users.models import AppUser


class Command(BaseCommand):
    """Custom command to create one hundred transactions"""
    help = 'To create transactions randomly'

    def handle(self, *args, **options):
        for i in range(100):
            people = Person.objects.all()
            users = AppUser.objects.all()

            person = people[randrange(0, people.count())]
            if person.cards.count() > 0:
                card = person.cards.all()[randrange(0, person.cards.all().count())]
                user = users[randrange(0, users.count())]
                Transaction.objects.create(
                    person=person,
                    card=card,
                    author=user,
                    amount=randrange(0, 200),
                )
