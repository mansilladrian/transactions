from datetime import date
from random import randrange

from django.test import TestCase
# Rest Framework
from django.urls import reverse
from rest_framework import status
from rest_framework.test import force_authenticate, APIRequestFactory, APITestCase

from apps.cards.api.v1.api import CardDetail
from apps.cards.models import Card, CardType, Person
from apps.users.models import AppUser


class CardApiTests(APITestCase):
    def setUp(self):
        self.user = AppUser.objects.create_user('adrian', '123456')
        CardType.objects.create(name='SQUA', rate=0.15, author=self.user)
        CardType.objects.create(name='SCO', rate=0.05, author=self.user)
        CardType.objects.create(name='PERE', rate=0.08, author=self.user)

        Person.objects.create(last_name='Romero', name='German', address='', dni='55824', author=self.user)
        Person.objects.create(last_name='Gutierres', name='Dario', address='', dni='210251', author=self.user)
        Person.objects.create(last_name='Jimenez', name='Sebastian', address='', dni='511520', author=self.user)
        Person.objects.create(last_name='Pabloski', name='Jose Luis', address='', dni='201536', author=self.user)

        Card.objects.create(
            name='GOLD',
            number=4444888899992222,
            due_date=date(2020, 4, 16),
            limit=2000,
            rate=1.5,
            holder='Adrian Mansilla',
            person_id=randrange(1, 5),
            brand_id=randrange(1, 4),
            author=self.user,
        )
        Card.objects.create(
            name='BLACK',
            number=343434343434343,
            due_date=date(2020, 3, 17),
            limit=2000,
            rate=1.5,
            holder='Adrian Mansilla',
            person_id=1,
            brand_id=1,
            author=self.user,
        )

    def test_get_detail(self):
        factory = APIRequestFactory()
        user = AppUser.objects.get(username='adrian')
        view = CardDetail.as_view()

        request = factory.get('/cards/api/v1/cards/2/')
        force_authenticate(request, user=user)
        response = view(request, pk='2')
        response.render()
        response_expected = b'{"name":"BLACK","number":"343434343434343","due_date":"2020-03-17","limit":2000.0,"rate":1.5,"holder":"Adrian Mansilla","person":{"name":"German","last_name":"Romero"},"brand":1}'
        self.assertEqual(response.content, response_expected)


class MethodTests(TestCase):
    """Testings internal methods"""
    def setUp(self):
        self.user = AppUser.objects.create_user('adrian', '123456')
        brand = CardType.objects.create(name='SQUA', rate=0.15, author=self.user)

        person = Person.objects.create(last_name='Romero', name='German', address='', dni='55824', author=self.user)

        self.card = Card.objects.create(
            name='GOLD',
            number=4444888899992222,
            due_date=date(2020, 4, 16),
            limit=2000,
            rate=1.5,
            holder='Adrian Mansilla',
            person=person,
            brand=brand,
            author=self.user,
        )

    def test_card_valid(self):
        print(self.card.due_date)
        self.assertEqual(self.card.is_valid, False)
