# utils
import uuid

# Django
from creditcards.models import CardNumberField
from django.core.exceptions import ValidationError
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from django.views.generic.dates import timezone_today


class Audit(models.Model):
    """Base model to get audit values for all tables"""
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)

    class Meta:
        abstract = True
        default_permissions = ()


class CardType(Audit):
    """Card type"""
    name = models.CharField(max_length=100)
    rate = models.FloatField(max_length=50)

    def __str__(self):
        """Return Card type name"""
        return self.name

    class Meta:
        ordering = ('name', )


class Person(Audit):
    """Person"""
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=150)
    dni = models.CharField(max_length=12)

    @property
    def get_full_name(self):
        return '{} {}'.format(self.name, self.last_name)

    def __str__(self):
        """ Return person name and last name """
        return '{} {}'.format(self.name, self.last_name)


class Card(Audit):
    """Card all related with a person's card"""
    name = models.CharField(max_length=50)
    number = CardNumberField()
    due_date = models.DateField()
    limit = models.FloatField()
    rate = models.FloatField()
    holder = models.CharField(max_length=100)
    person = models.ForeignKey(Person, on_delete=models.PROTECT, related_name='cards')
    brand = models.ForeignKey(CardType, on_delete=models.PROTECT, related_name='cards')

    def __str__(self):
        """ Return card's number/due date """
        return '{}/{}'.format(self.number, self.due_date)

    @property
    def is_valid(self):
        """Check if the card is valid, a valid card is when its due date is lower than the current date"""
        return self.due_date > timezone_today()

    @property
    def get_rate(self):
        """Get card's brand and its rate to be returned in a json format."""
        return self.brand.rate

    def clean(self):
        """Checking if the card data is already used."""
        card = Card.objects.filter(
            brand=self.brand,
            number=self.number,
            holder=self.holder,
            due_date=self.due_date,
        )
        if card.exists():
            raise ValidationError(_('This card data is already used.'))


class Transaction(Audit):
    """Transaction"""
    person = models.ForeignKey(Person, on_delete=models.PROTECT, related_name='transactions')
    card = models.ForeignKey(Card, on_delete=models.PROTECT, related_name='transactions')
    amount = models.FloatField()
    amount_rate = models.FloatField(default=0)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __str__(self):
        """ Return transaction amount. """
        return self.amount

    @property
    def is_valid(self):
        """Transaction is valid if amount is not grater than one hundred."""
        return 0 < self.amount < 100

    @property
    def card_brand(self):
        """Name of the brand which is used on the transaction"""
        return self.card.brand.name

    @property
    def get_rate(self):
        """Operation to get rate, brand and amount"""
        return 'Rate: {}, Brand: {}, Amount {}'.format(self.card.brand.rate, self.card_brand, self.amount)

    def save(self, *args, **kwargs):
        """Save method
         Calculate amount rate
         """
        self.amount_rate = self.amount * self.card.brand.rate
        return super(Transaction, self).save(*args, **kwargs)