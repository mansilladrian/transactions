# Django
from django.urls import path, include

# Views
from apps.cards.views import (
    Dashboard,
    ListCards,
    NewCard,
    ListBrands,
    NewBrand,
    UpdateBrand,
    UpdateCard,
    ListPeople,
    NewPerson,
    UpdatePerson,
    ListTransactions,
    ListPersonCards
)

urlpatterns = [
    path('dashboard', Dashboard.as_view(), name="dashboard"),

    path('list_cards', ListCards.as_view(), name="list_cards"),
    path('new_card', NewCard.as_view(), name="new_card"),
    path('update_card/<int:pk>', UpdateCard.as_view(), name="update_card"),

    path('list_brands', ListBrands.as_view(), name="list_brands"),
    path('new_brand', NewBrand.as_view(), name="new_brand"),
    path('update_brand/<int:pk>', UpdateBrand.as_view(), name="update_brand"),

    path('list_people', ListPeople.as_view(), name="list_people"),
    path('new_person', NewPerson.as_view(), name="new_person"),
    path('update_person/<int:pk>', UpdatePerson.as_view(), name="update_person"),
    path('list_person_cards/<int:pk>', ListPersonCards.as_view(), name="list_person_cards"),


    path('list_transactions', ListTransactions.as_view(), name='list_transactions'),
    # API
    path('api/', include('apps.cards.api.urls')),
]
