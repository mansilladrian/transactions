# Django
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import FormView, RedirectView, ListView, UpdateView

# Models
from apps.users.forms import UserForm, UpdateUserForm, UpdatePasswordForm
from apps.users.models import AppUser


class LoginView(FormView):
    """Login view"""
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = reverse_lazy("dashboard")

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)


class LogoutView(LoginRequiredMixin, RedirectView):
    """Logout view"""
    pattern_name = 'login'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class ListUsers(LoginRequiredMixin, ListView):
    """List users"""
    template_name = 'users/list_users.html'
    queryset = AppUser.objects.all()
    context_object_name = 'users'


class NewUser(LoginRequiredMixin, FormView):
    """To create new user"""
    template_name = 'users/user_form.html'
    form_class = UserForm
    success_url = reverse_lazy('users')

    def form_valid(self, form):
        """Form valid"""
        form.save()
        return super(NewUser, self).form_valid(form)


class UpdateUser(LoginRequiredMixin, UpdateView):
    """Edit user view"""
    model = AppUser
    template_name = 'users/user_form.html'
    form_class = UpdateUserForm
    slug_url_kwarg = 'username'
    slug_field = 'username'
    success_url = reverse_lazy('users')

    def get_context_data(self, **kwargs):
        """Context"""
        context = super(UpdateUser, self).get_context_data(**kwargs)
        context['edited_user'] = AppUser.objects.get(username=self.kwargs['username'])
        return context
    
    def form_valid(self, form):
        """Form valid"""
        form.save()
        return super(UpdateUser, self).form_valid(form)


class UpdatePassword(LoginRequiredMixin, FormView):
    """Update password and username view"""
    # model = AppUser
    template_name = 'users/change_password.html'
    form_class = UpdatePasswordForm
    slug_url_kwarg = 'username'
    slug_field = 'username'
    success_url = reverse_lazy('users')
    user = None

    def dispatch(self, request, *args, **kwargs):
        self.user = AppUser.objects.get(username=self.kwargs['username'])
        return super(UpdatePassword, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        """Handle form's kwargs"""
        kwargs = super(UpdatePassword, self).get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

    def form_valid(self, form):
        """Form valid"""
        user = self.user
        user.username = form.cleaned_data['username']
        user.set_password(form.cleaned_data['password2'])
        user.save()
        return super(UpdatePassword, self).form_valid(form)
