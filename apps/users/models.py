from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class AppUserManager(BaseUserManager):
    def create_user(self, username, password): # , email):
        """Create user"""
        user = self.model(username=username)
        user.set_password(password)
        # user.email = email
        user.save()
        return user

    def create_superuser(self, username, password): # , email):
        """Create superuser"""
        user = self.create_user(username, password)#, email)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class AppUser(AbstractBaseUser, PermissionsMixin):
    """User model."""
    created = models.DateTimeField(auto_now_add=True, null=True)
    modified = models.DateTimeField(auto_now=True)

    first_name = models.CharField(
        verbose_name='First Name',
        max_length=255,
        null=True,
        blank=True
    )
    last_name = models.CharField(
        verbose_name='Last Name',
        max_length=255,
        null=True,
        blank=True
    )

    # email = models.EmailField(
    #     verbose_name='email address',
    #     max_length=255,
    #     unique=True,
    # )

    username = models.CharField(max_length=255, unique=True)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = AppUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        """Return the full name for this User."""
        return '%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __str__(self):
        """Return username"""
        return self.username

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        ordering = ['username']
        db_table = 'auth_users'
