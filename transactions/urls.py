"""transactions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# Django
from django.contrib import admin
from django.urls import path, include

# Views
from apps.users.views import LoginView, LogoutView, ListUsers, NewUser, UpdateUser, UpdatePassword

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('users/', ListUsers.as_view(), name='users'),
    path('new_user/', NewUser.as_view(), name='new_user'),
    path('update_user/<slug:username>', UpdateUser.as_view(), name='update_user'),
    path('update_password/<slug:username>', UpdatePassword.as_view(), name='update_password'),
    path('cards/', include('apps.cards.urls')),
]
