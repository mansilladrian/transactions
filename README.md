# Transactions App


## Dependencies
Python 3
Django

## Usage
```shell
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.txt
```
You need to create local.py file in: 
> transactions/settings/local.py

####Ejemplo:

```python
from .base import *
from .base import env

DEBUG = True

# Security
SECRET_KEY = env('DJANGO_SECRET_KEY', default='o9@(+q+pp(5qeso@@@x7+%y!o%&q+8d2%*!9qtx3j@swa%1vv%')

ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'transaction',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
```
```python
python manage.py makemigrations
python manage.py migrate
python managae.py compress
python manage.py runserver
```

## Generate transactions
```shell
python manage insert_random_transactions
```
## License
[This project is under MIT License](https://opensource.org/licenses/MIT)

## Author
[Adrian Mansilla](https://gitlab.com/mansilladrian) | 2021